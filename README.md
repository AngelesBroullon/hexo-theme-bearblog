# Hexo-Bearblog

🧸 An Hexo-theme based [Bear Blog](https://bearblog.dev/).

>  Free, no-nonsense, super-fast blogging.

* Very light.
* Simple template.
* Language highlighting is not included to speed it up, since it's a lot a CSS.
* Supports light and dark mode on web browser.
* Support for English and Spanish language.
* Has capabilities for having a search page.
* Supports tags to prevent scrapping for AIs (`noai`, `noimageai`).
* Support fediverse creator authoring.

## Installation

1. Clone this repository into your Hexo theme folder.
2. Edit the `_config.yml` file:
    ```yml
    theme: hexo-bearblog
    ```

### Configure local search

Bear in mind that this can be a much heavier page, depending on the size of the site.

1. Install the `hexo-generate-search` plugin, which will generate a search index file.

    ```sh
    $ npm install hexo-generator-searchdb --save
    ```

2. Create a page to display the search engine.

    ```sh
    $ hexo new page search
    ```

3. Edit the page type, so it has searching in the front-matter.

    ```yml
    title: Search
    type: search
    ---
    ```

4.  Edit the main `_config.yml`.

    ```yml
    search:
        path: search.json
        field: post
        content: false # to keep it light
        format: html
    ```

## Recognition

The structure of this theme was inspired by:
* Hexo cactus theme
* Hexo minima theme

## Licence

GNU GPLv3
