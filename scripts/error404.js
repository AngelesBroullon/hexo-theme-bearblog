/**
* error 404 page Generator
* @description generate the 404.html in root directory
*/
hexo.extend.generator.register('error_404', function (locals) {
  return {
    path: '404.html',
    data: locals.posts,
    layout: '404'
  }
})

/**
 * Reroute non existing pages to 404 page
 */
hexo.extend.filter.register('server_middleware', function(app){
  app.use(function handle404(req, res, next) {
    res.writeHead(302, {
      'Location': hexo.config.url + hexo.config.root + '/404'
    });
    res.end();
  });
}, 99)